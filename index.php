<?php
/*

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/********
* v1.0.0
*********/

include 'config.php';

// Enable debugging
ini_set ( 'display_errors', 1 );

// Include Twig
require_once './lib/Twig/Autoloader.php';
Twig_Autoloader::register();

// Load Twig
$loader = new Twig_Loader_Filesystem ( TEMPLATES_PATH );
$settings = array();
if ( CACHE_PATH )
	$settings['cache'] = CACHE_PATH;
$twig = new Twig_Environment ( $loader, $settings );

// Get the URL path
$template_name = ltrim ( substr ( $_SERVER['REQUEST_URI'], strlen ( substr ( dirname ( $_SERVER['SCRIPT_FILENAME'] ), strlen ( $_SERVER['DOCUMENT_ROOT'] ) ) ) ), '/' );

// If it's empty, use 'index'
if ( strlen ( $template_name ) < 1 )
	$template_name = 'index';

$template = TEMPLATES_PATH . '/' . $template_name;

// If it's a directory, append 'index'
if ( is_dir ( $template ) ) {
	$template_name = rtrim ( $template_name, '/' );
	$template_name .= '/index';
}

// FIXME repeated code
$template = TEMPLATES_PATH . '/' . $template_name;

// If it's not a directory or a file, try to find it
if ( !is_file ( $template ) ) {
	$extensions = array ( '.html.twig', '.html' );

	foreach ( $extensions as $extension ) {
		if ( is_file ( $template . $extension ) ) {
			$template_name .= $extension;
			break;
		}
	}

}

// Render the template
echo $twig->render ( $template_name, unserialize ( GLOBAL_VARS ) );
