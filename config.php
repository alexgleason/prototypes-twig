<?php

// Location of the template files
define ( 'TEMPLATES_PATH', './templates' );

// Location of the cache, or false to disable cache
define ( 'CACHE_PATH', false );

// Global variables
define ( 'GLOBAL_VARS', serialize ( array (
	'siteUrl' => '//' . dirname ( $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] ) . '/'
)));
