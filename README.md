Twig Prototyping Base
=====================
Includes a modified version of [lanaluhas/web-framework](https://github.com/lanaluhas/web-framework) which is written in Twig and compiled at runtime. Also includes a Vagrantfile to easily create an environment.


How To
------
Clone with `git clone --recursive git@github.com:alexgleason/prototypes-twig.git`.

	cd path/to/repo
	vagrant up

Then browse to `localhost:8080` in your web browser.

To stop the Vagrant server, use:

	vagrant halt

Advanced Commands
-----------------
To run the following commands, first shell into Vagrant.

	vagrant ssh

To begin a SASS watcher, type:

	sass-watch

To build a static HTML version of the prototypes, type:

	build

Folder Tree
-----------
<pre><code>
.
├── assets
│   ├── scripts
│   └── styles
├── lib
│   └── Twig    # Twig library
├── media
│   └── images
├── templates
├── config.php  # Compiler settings
├── index.php   # The compiler
└── README.md
</code></pre>

Included Software
=================

Twig
----
Copyright (c) 2009-2014 by the Twig Team

**Path:** lib/Twig

Foundation
----------
Copyright (c) 2013-2015 ZURB, inc.

**Path:** assets/styles/sass/foundation
